$(function () {

    // Remove Map Image
    $('#map').removeClass('nojs');

    /**
     * Send contact form
     */
    $("#contact-btn").click(function (e) {
        e.preventDefault();
        var contact = {};
        contact.name = $('#name').val();
        contact.email = $('#email').val();
        contact.comment = $('#comment').val();

        // Attempts to send contact message
        $.ajax({
            type: 'post',
            url: '/contact.php',
            data: contact,
            success: function (data) {
                $('#form').trigger('reset');
                $('#form_result > div > p').html(data);
                $('#form_result').removeClass('hide error').addClass('success');
            },
            error: function (error) {
                $('#form_result > div > p').html(error.responseText);
                $('#form_result').removeClass('hide success').addClass('error');
            }
        });
    });

});
