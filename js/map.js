function initMap() {
    var myLatLng = {
        lat: 51.494335,
        lng: -0.061212
    };

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 16,
        center: myLatLng,
        styles: [{
            stylers: [{
                saturation: -100
            }]
        }],
        scrollwheel: false
    });

    var marker = new google.maps.Marker({
        position: myLatLng,
        title: "Paella Intelligence",
        icon: "../images/location.png"
    });

    // To add the marker to the map, call setMap();
    marker.setMap(map);
}
