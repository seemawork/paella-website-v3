$(function () {
    var offset = 250;
    var duration = 500;

    $(window).scroll(function () {

        // Makes the "back-to-top" button appear/disappear
        if ($(this).scrollTop() > offset) {
            $(".back-to-top").addClass("active");
        } else {
            $(".back-to-top").removeClass("active");
        }
    });

    // Scroll to the top of the page
    $(".back-to-top").click(function (event) {
        event.preventDefault();

        $("html, body").animate({
            scrollTop: 0
        }, duration);

        return false;
    });
});
