window.cookieconsent_options = {
    "message": "This website has cookies to ensure you get the best experience on our website; for more info click",
    "dismiss": "Got it!",
    "learnMore": "here",
    "link": "/cookie-policy",
    "theme": "dark-bottom",
    "target": "_blank"
};
