<?php
    error_reporting(0);

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $name = htmlspecialchars(strip_tags($_POST["name"]));
        $email = htmlspecialchars(strip_tags($_POST["email"]));
        $comment = htmlspecialchars(strip_tags($_POST["comment"]));
        $error = '';

        // Empty Fields Check
       if( empty($name) || empty($email) || empty($comment) ) {
           $error = "Please, fill in all the fields.";

        } else {
          // Valid Email Check
          if(filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            $error = "Please enter a valid email address";
          }
        }

        // No Errors
       if( empty($error) ) {
          $to = "office@paellaintelligence.com";
          $subject = "Contact Form Message from " . $name;
          $headers = "From: $email" . "\r\n";

          // Send Mail and return success message
          if( mail($to,$subject,$comment,$headers) ) {
            http_response_code(200);
            echo "Message sent successfully!";

          // Return server error
          } else {
              http_response_code(500);
              echo "Message failed to send; Please try again later";
          }

        // Return user errors
        } else {
          http_response_code(400);
          echo $error;
        }

    }
?>
